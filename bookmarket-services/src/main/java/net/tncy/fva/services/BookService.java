package net.tncy.fva.services;

import net.tncy.fva.data.Book;
import net.tncy.fva.data.Bookstore;

import java.util.ArrayList;

public class BookService{

    public ArrayList<Book> retrieveBooks(){
        ArrayList<Bookstore> librairies = new ArrayList<Bookstore>();
        ArrayList<Book> livres = new ArrayList<Book>();
        for (Bookstore librairie: librairies){
            livres.addAll(librairie.getLivres());
        }
        return livres;
    }

}